import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoffee } from '@fortawesome/free-solid-svg-icons'
import { Link } from 'react-router-dom'
import Style from './Header.module.scss'

const Header = () => {
  return (
    <div className={Style.header}>
      <dev className={Style.item}>
        <Link to="/">VideoTube</Link>
      </dev>
      <dev className={Style.item}>
        <form>
          <input type='text' placeholder='検索'/>
          <button type='submit'> <FontAwesomeIcon icon={faCoffee} /></button>
        </form>
      </dev>
    </div>
  )
}

export default Header