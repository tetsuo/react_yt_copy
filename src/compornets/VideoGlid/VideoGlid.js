import React from 'react'
import Style from './VideoGlid.module.scss'

const VideoGlid = ({children}) => {
  return (
    <div className={Style.container}>
      {children}
    </div>
  )
}

export default VideoGlid