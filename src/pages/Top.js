import React, { useContext, useEffect } from "react";
import Layout from "../compornets/Layout/Layout";
import { fetchPopularData } from "../apis/index";
import { Store } from "../store/index";
import VideoGlid from "../compornets/VideoGlid/VideoGlid";
import VideoGlidItem from "../compornets/VideoGlidItem/VideoGlidItem";
const Top = () => {
  const { globalState, setGlobalState } = useContext(Store);

  useEffect(() => {
    fetchPopularData().then((res) => {
      console.log('data', res)
      setGlobalState({type: 'SET_POPULAR', payload: {popular: res.data.items}})
    })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <Layout>
      <VideoGlid>
        {globalState.popular && globalState.popular.map((popular) => {
            return (
              <VideoGlidItem
                id={popular.id}
                key={popular.id}
                src={popular.snippet.thumbnails.standard.url}
                titel={popular.snipet.titel}
              />
            );
          })}
      </VideoGlid>
    </Layout>
  );
};

export default Top;
