import React, {createContext, useReducer } from 'react'

const initialState = {
  populer: []
}

const reducer = (state, action) => {
  switch(action.type) {
    case 'SET_POPULAR':
      return { populer: action.payload.populer }
    default:
      return state
  }
}

export const Store = createContext({
  globalState: initialState,
  setGlobalState: () => null
})

const StoreProveider = ({children}) => {
  const {globalState, setGlobalState} = useReducer(reducer, initialState)
  return (
    <Store.Provider value={{ globalState, setGlobalState }}>{children}</Store.Provider>
    )
  }

export default StoreProveider